module gitlab.com/kapatalayan/migration

go 1.13

require (
	github.com/alecthomas/log4go v0.0.0-20180109082532-d146e6b86faa
	github.com/gin-gonic/gin v1.5.0 // indirect
	github.com/jinzhu/gorm v1.9.12
	gitlab.com/kapatalayan/models v1.0.1
)
