package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	m "gitlab.com/kapatalayan/models"
)

const version string = "1.0.0"

// AppConfig for program
type AppConfig struct {
	DatabaseHost     string `json:"dbHost"`
	DatabaseName     string `json:"dbName"`
	DatabasePassword string `json:"dbPwd"`
	DatabasePort     int    `json:"dbPort"`
	DatabaseUser     string `json:"dbUser"`
	Debug            bool   `json:"debug"`
}

var appConfig AppConfig
var argConfig string
var argToVersion int
var argVersion bool

func init() {
	flag.BoolVar(&argVersion, "v", false, "print application version")
	flag.IntVar(&argToVersion, "t", 99999, "switch version")
	flag.StringVar(&argConfig, "c", "app.json", "configuration file")
}

func main() {
	flag.Parse()

	if argVersion {
		fmt.Println(version)
		return
	}

	var err error

	if appConfig, err = getAppConfig(); err != nil {
		fmt.Printf("can not get app config: %v\n", err)
		return
	}

	var db *gorm.DB
	if db, err = m.ConnectToDatabase(appConfig.DatabaseHost, appConfig.DatabaseUser, appConfig.DatabaseName, appConfig.DatabasePassword, appConfig.DatabasePort); err != nil {
		fmt.Printf("connect database fail: %v\n", err)
		return
	}
	defer db.Close()

	if appConfig.Debug {
		db.LogMode(true)
	}

	if err = m.Migration(db, argToVersion); err != nil {
		fmt.Printf("execut migration fail: %v\n", err)
	}
}

func getAppConfig() (appConfig AppConfig, err error) {
	var jsonByte []byte
	if jsonByte, err = ioutil.ReadFile("app.json"); err != nil {
		return
	}

	err = json.Unmarshal(jsonByte, &appConfig)

	return
}

func getConnection() (db *gorm.DB, err error) {
	db, err = m.ConnectToDatabase(appConfig.DatabaseHost, appConfig.DatabaseUser, appConfig.DatabaseName, appConfig.DatabasePassword, appConfig.DatabasePort)
	if err != nil {
		fmt.Printf("could not connect to server: %v\n", err)
		return
	}

	if appConfig.Debug {
		db.LogMode(true)
	}

	return
}
